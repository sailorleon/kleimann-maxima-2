package Homework20;

import Homework20.User;
import Homework20.UsersRepositoryFileBasedImpl;

import java.io.IOException;

/**
 * 29.01.2022
 * 25. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws IOException {
        UsersRepositoryFileBasedImpl usersRepository = new UsersRepositoryFileBasedImpl("users.txt");
        User marsel = new User("Марсель", "Сидиков");
        User maxim = new User("Максим", "Анисимов");
        User ravil = new User("Равиль", "Фахрутдинов");

        usersRepository.save(marsel);
        usersRepository.save(maxim);
        usersRepository.save(ravil);
        System.out.println(usersRepository.findAll());
    }
}
