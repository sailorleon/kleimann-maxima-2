package Homework20;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 29.01.2022
 * 25. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersRepositoryFileBasedImpl {

    private String fileName;

    public UsersRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    // возвращает список всех пользователей из файла
    public List<User> findAll() throws IOException {
        List<User> userList = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            userList.add(new User(line));
        }
        bufferedReader.close();
        return userList;
    }


    public Optional<User> findByFirstName(String firstName) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String userLine = reader.readLine();

            while (userLine != null) {
                String[] parsedUserLine = userLine.split("\\|");

                if (parsedUserLine[0].equals(firstName)) {
                    String firstFileName = parsedUserLine[0];
                    String lastNameFromFile = parsedUserLine[1];
                    User user = new User(firstFileName, lastNameFromFile);
                    return Optional.of(user);
                }
                // если if-не сработал, читаем новую строку
                userLine = reader.readLine();
            }
            // если дошли до конца файла и ничего не нашли, возвращаем пустой Optional
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    public void save(User user) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.write(user.getFirstName() + "|" + user.getLastName());
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            // выполняется всегда, независимо от ошибок
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
        }
    }
}
