package Homework17;

import java.util.*;


public class Bank {
    private Map<User, List<Transaction>> transactions;

    public Bank() {
        this.transactions = new HashMap<>();
    }

    // метод для перевода денег
    public void sendMoney(User from, User to, int sum) {
        // создаем сам перевод
        Transaction transaction = new Transaction(from, to, sum);
        // проверить, если ли уже у нас история переводов этого пользователя или нет
        // если еще переводов не было
        if (!transactions.containsKey(from)) {
            // кладу пользователю пустой список
            transactions.put(from, new ArrayList<>());
        }
        // get - получает список всех транзакций пользователя from и добавляет транзакцию в этот список
        transactions.get(from).add(transaction);
    }

    public List<Transaction> getTransactionsByUser(User user) {
        return transactions.get(user);
    }

    // вернуть сумму всех транзакций по конкретному пользователю
    public int getTransactionsSum(User user) {

        List<Transaction> list = getTransactionsByUser(user);
        int sum = 0;
        for (Transaction transaction : list) {
            sum += transaction.getSum();
        }
        return sum;
    }
    //вывести имя пользователя и количество его транзакций
    public void printAllUsers() {
        for (Map.Entry<User, List<Transaction>> entry : transactions.entrySet()) {
            System.out.println(entry.getKey().getName() + " - " + entry.getValue().size());
        }
    }

    public void printUser() {
        Set<User> set = transactions.keySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());

        }
    }

}

