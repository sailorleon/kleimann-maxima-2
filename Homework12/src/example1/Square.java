package example1;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// квадрат
public class Square extends Rectangle {
    public Square(int x, int y, int a) {
        super(x, y, a, a);
    }

    @Override
    public double getPerimeter() {
        return a * 4;
    }

}

