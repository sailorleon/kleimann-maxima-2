package example1;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// ДЛИНА ОКРУЖНОСТИ
public class Circle extends Ellipse{
    private double radius;

    public Circle(int x, int y, int radius) {
        super(x, y, radius, radius);
        this.radius = radius;
    }

    @Override
    public double getPerimeter() {
        return (float) (2*Math.PI * radius); // вычисляем и возвращаем длину окружности
    }
}
