package example1;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// овал
public class Ellipse extends Figure {
    private int radiusF;
    private int radiusS;

    public Ellipse(int x, int y, int radiusF, int radiusS) {
        super(x, y);
        this.radiusF = radiusF;
        this.radiusS = radiusS;
    }

    @Override
    public double getPerimeter() {
        return 4 * (Math.PI * radiusF * radiusS + Math.pow((radiusF - radiusS), 2)) / (radiusF + radiusS);
    }
}

