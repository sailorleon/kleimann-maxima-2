package example1;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public abstract class Figure {

    private int x; // 1-я координата "центра" фигуры
    private int y; // 2-я координата "центра" фигуры

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public abstract double getPerimeter(); // объвление метода вычисления периметра

    // Объявление метода перемещения центра фигуры
    public void move(int newX, int newY){
        this.x += newX;
        this.y += newY;
    }

    // метод, который печатает координаты центра фигуры
    public String printCenter(){
        return "x: " + x + " y: " + y;
    }
}

