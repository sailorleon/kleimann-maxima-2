package example1;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(0, 0, 4, 7); // прямоугольник со сторонами 5 и 7
        Square square = new Square(0, 0, 4); // квадрат со стороной 4
        Ellipse ellipse = new Ellipse(0, 0, 4, 5);
        Circle circle = new Circle(0, 0, 6); // Окружность с радиусом 6
        System.out.println("Периметр прямоугольника равен " + rectangle.getPerimeter() + "; Координаты:  " + rectangle.printCenter());
        System.out.println("Периметр квадрата равен -  " + square.getPerimeter() + "; Координаты: " + square.printCenter());
        System.out.println("Периметр эллипса равен = " + ellipse.getPerimeter() + "; Координаты: " + ellipse.printCenter());
        System.out.println("Длина окружности равна = " + circle.getPerimeter() + "; Координаты: " + circle.printCenter());

        rectangle.move(66, 77);
        square.move(66, 77);
        ellipse.move(66, 77);
        circle.move(66, 77);
        System.out.println("");
        System.out.println("Двигаем фигуры на плоскости");
        System.out.println("Теперь координаты центра прямоугольника такие: " + rectangle.printCenter());
        System.out.println("Теперь координаты центра квадрата такие: " + square.printCenter());
        System.out.println("Теперь координаты центра эллипса такие: " + ellipse.printCenter());
        System.out.println("Теперь координаты центра круга такие: " + circle.printCenter());
        System.out.println();
        System.out.println("Передвигаем центр круга в точку с координатами 10 10");
        circle.move(-56, -67);
        System.out.println("Теперь центр круга расположен в точке " + circle.printCenter());

    }
}

