create table products (
id serial primary key,
name char(100),
price float4,
expiration_date date,
date_of_receipt date,
vendor char(100)
);

insert into products (name, price,expiration_date, date_of_receipt,vendor
) values ('Сгущенка', 150, '01.01.2030', '01.01.2022', 'Вимбильдан');

insert into products (name, price,expiration_date, date_of_receipt,vendor
) values ('Сыр', 250, '01.01.2023', '01.01.2020', 'Веселый молочник');

insert into products (name, price,expiration_date, date_of_receipt,vendor
) values ('Молоко', 90, '01.05.2021', '01.02.2022', 'Домик в деревне');

insert into products (name, price,expiration_date, date_of_receipt,vendor
) values ('Ряженка', 120, '01.05.2021', '01.02.2022', 'Вимбильдан');

insert into products (name, price,expiration_date, date_of_receipt,vendor
) values ('Йогурт', 60, '01.03.2022', '05.02.2022', 'Danone');

select * from products where price > 100;

select * from products where date_of_receipt::date < '02.02.2020'::date;

select vendor from products;
