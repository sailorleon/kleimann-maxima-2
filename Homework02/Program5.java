import java.util.Scanner;

class Program5 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		//System.out.println("Введите целое число от 0 до 127: ");
		int number = scanner.nextInt();

		System.out.println("Вы ввели число: "  + number);

		int n1;
		int n2;
		int n3;
		int n4;
		int n5;
		int n6;
		int n7;
		int n8;

		// ...

		// необходимо вывести двоичное представление числа number

		if (0 <= number & number <= 127) 
		{

		n1 = number%2; // первая цифра
		System.out.println("Цифра: " + n1);

		number = number/2; //результат деления
		System.out.println("Результат деления:" + number);
		
		n2 = number%2;
		System.out.println("Цифра: " + n2);
		
		number = number/2;
		System.out.println("Результат деления:" + number);

		n3 = number%2;
		System.out.println("Цифра: " + n3);

		number = number/2;
		System.out.println("Результат деления:" + number);

		n4 = number%2;
		System.out.println("Цифра: " + n4);

		number = number/2;
		System.out.println("Результат деления:" + number);

		n5 = number%2;
		System.out.println("Цифра: " + n5);

		number = number/2;
		System.out.println("Результат деления:" + number);

		n6 = number%2;
		System.out.println("Цифра: " + n6);

		number = number/2;
		System.out.println("Результат деления:" + number);

		n7 = number%2;
		System.out.println("Цифра: " + n7);

		number = number/2;
		System.out.println("Результат деления:" + number);

		n8 = number%2;
		System.out.println("Цифра: " + n8);

		System.out.println("Двоичное представление: " + n8 + "" + n7 + "" + n6 + "" + n5+ "" + n4 + "" + n3 + "" + n2 + "" + n1);
		}
		else System.out.println("Вы ввелии некорректное число.");
	}
}

// Выполнить деление исходного числа на 2. 
//Если результат деления больше или равен 2, 
//продолжать делить его на 2 до тех пор, пока результат деления не станет равен 1.
// Выписать результат последнего деления и все остатки от деления в обратном порядке в одну строку.

//Гарантируется, что 0 <= number <= 127
//Нельзя использовать массивы, циклы, строки, Integer.