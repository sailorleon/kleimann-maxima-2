public class Main {

    // функция, для которой считаем определенный интеграл - y = x^2 (парабола), y = sin(x) * 2
    public static double f(double x) {
        // return x*x;
        return Math.sin(x) * 2;
    }

    // МЕТОД ПРЯМОУГОЛЬНИКОВ
    // функция, которая возвращает значение определенного интеграла для функции f(x) на промежутке от 0 до 10 с разбиением n
    public static double integralByRectangles(double a, double b, int n) {
        // ширина одного прямоугольника
        double width = (b - a) / n;
        // общая сумма площадей всех прямоугольников
        double sum = 0;

        for (double x = a; x <= b; x += width) {
            // считаем значение функции в текущей точке
            double height = f(x);
            // вычисляем площадь текущего прямоугольника
            double currentRectangleArea = height * width;
            // кидаем в общую сумму всех площадей всех прямоугольников
            sum += currentRectangleArea;
        }
        return sum;
    }

    // TODO: реализовать
    // МЕТОД Симпсона
    // функция, которая возвращает значение определенного интеграла для функции f(x) на промежутке от 0 до 10 с разбиением n
    public static double integralBySimpson(double a, double b, int n) {
        //        return 0.0;
        double h = (b - a) / n; // ширина интервала разбиения
        // значение интеграла методом Симпсона
        double sumSimpson = 0;
        for (double x = a; x <= b; x += h) {
            double m = (x + (x+h))/2;
            double currentArea = (h/3) * (f(x) + 4*f(m) + f(x+h))/2;
            sumSimpson += currentArea;
         }
        return sumSimpson;
    }

    public static void main(String[] args) {
        // массив с примерами количеств разбиений исходного диапазона
        int[] ns = {10, 100, 1000, 10_000, 50_000, 100_000,
                150_000, 200_000, 300_000, 500_000, 1_000_000, 2_000_000};
        double from = 0;
        double to = 10;
        double realValue = 3.67814305815;
        // double realValue = 333.33;

        // массив с результатами работы функции integral для разных разбиений, в ys[i] находится значение для разбиения ns[i]
        double[] ys = new double[ns.length];
        // считаем интегралы для всех разбиений на промежутке от 0 до 10
        for (int i = 0; i < ns.length; i++) {
            // ys[i] = integralByRectangles(from, to, ns[i]);
            ys[i] = integralBySimpson(from, to, ns[i]);
        }
        // вывод результатов
        for (int i = 0; i < ns.length; i++) {
            System.out.printf("|N = %10d| Y = %10.4f| EPS = %10.5f |\n", ns[i], ys[i], Math.abs(ys[i] - realValue));
        }
    }
}
