import java.util.Scanner;

class Program1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);	

		int[] array = new int[10]; // объявил массив

		// Заполнение массива

		for (int i = 0; i < 10; i++) {

			array[i] = scanner.nextInt();	
		}

		// Вывод массива в обратном порядке

		System.out.println("Вывод массива в обратном порядке:");
		int count = 9;	

		for (int i = 0; i < 10; i++) {
			System.out.println(array[count]);
			count = count - 1;
		}
	}
}

// СДЕЛАНО

// Объявить массив (размер - 10). Заполнить его элементами из консоли.
// Вывести в обратном порядке.
