package Homework14;


public class LinkedList<E> implements List<E> {


    private static class Node<V> {
        V value;
        Node<V> next;
        Node<V> prev;

        Node(V value) {
            this.value = value;
        }
    }

    // ссылка на первый элемент
    private Node<E> first;
    // ссылка на последний элемент
    private Node<E> last;


    private int size = 0;

    @Override
    public int size() {
        return size;
    }

    public void add(E element) {
        // для нового элемента создаем узел
        Node<E> newNode = new Node<>(element);
        // если список пустой
        if (isEmpty()) {
            // первый узел = новый узел
            first = newNode;
            last = newNode;
        } else {
            // следующий после последнего - это новый узел
            newNode.prev = last;
            last.next = newNode;
        }
        // теперь новый узел - последний
        last = newNode;

        size++;
    }

    public void addToBegin(E element) {
        // для нового элемента создаем узел
        Node<E> newNode = new Node<>(element);

        if (isEmpty()) {
            last = newNode;
        } else {
            // для нового узла, следующий после него - это первый узел списка
            newNode.next = first;
        }

        // теперь новый узел - первый
        first = newNode;
        size++;
    }

    public E get(int index) {
        // начинаем с первого элемента
        Node<E> current = first;

        int i = 0;

        while (i < size) {
            // если нашли нужный индекс
            if (i == index) {
                // возвращаем значение текущего узла
                return current.value;
            }
            // иначе идем дальше
            current = current.next;
            i++;
        }
        throw new NullPointerException("В списке нет элемента под таким индексом");
    }

    public void removeAt(int index) {
        Node<E> previousRemove = first;

        if (index == 0) {
            // если удаляется первый элемент, просто сдвигаем ссылку на него
            first = first.next;
            size--;
            return;
        }

        // доходим до индекса элемента, который стоит перед удаляемым
        for (int i = 0; i < index - 1; i++) {
            // переходим по узлам
            previousRemove = previousRemove.next;
        }
        // если удаляем последний
        if (index == (size - 1)) {
            last = previousRemove;
        }
        // в current находится узел, который стоит перед удаляемым
        Node<E> forRemove = previousRemove.next;
        previousRemove.next = forRemove.next;

        size--;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void remove(E element) {
        Node<E> forRemove = first;
        int currentIndex = 0;
        while (forRemove != null) {
            if (forRemove.value.equals(element)) {
                removeAt(currentIndex);
                return;
            }
            forRemove = forRemove.next;
            currentIndex++;
        }
    }


    public void removeLastEntry(E element) {
        Node<E> forRemove = last;
        int currentIndex = size - 1;
        while (forRemove != null) {
            if (forRemove.value.equals(element)) {
                removeAt(currentIndex);
                return;
            }
            forRemove = forRemove.prev;
            currentIndex--;
        }
    }


    public void removeAll(E element) {
        Node<E> forRemove = first;
        int currentIndex = 0;
        while (forRemove != null) {
            if (forRemove.value.equals(element)) {
                remove(element);
            }
            forRemove = forRemove.next;
            currentIndex++;

        }

    }

    public void add(int index, E element) {
        if (index >= size) {
            System.out.println("Неподходящий индекс!!!");
            return;
        }
        if (index == 0) {
            addToBegin(element);
            return;
        }

        Node<E> forAdd = first;
        Node<E> newNode = new Node<>(element);
        int currentIndex = 0;
        while (currentIndex < index - 1) {
            forAdd = forAdd.next;
            currentIndex++;

        }
        Node<E> nextNode = forAdd.next;
        forAdd.next = newNode;
        newNode.next = nextNode;

        size++;
    }
}

