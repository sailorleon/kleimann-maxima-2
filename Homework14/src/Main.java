package Homework14;


public class Main {
    public static void main(String[] args) {
        ArrayList <String> list = new ArrayList<>();
        list.add("Январь");
        list.add("Февраль");
        list.add("Январь");
        list.add("Март");
        list.add("Апрель");
        list.add("Май");
        list.add("Сентябрь");
        list.add("Июнь");
        list.add("Июль");
        list.add("Сентябрь");
        list.add("Август");
        list.add("Сентябрь");
        list.add("Октябрь");
        list.add("Ноябрь");
        list.add("Декабрь");
        list.get(2);

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        list.removeAt(1);
        list.remove("Январь");
        list.removeLastEntry("Январь");
        list.removeAll("Сентябрь");
        list.add(3, "Отпуск");
        System.out.println();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println();

        LinkedList <Integer> linkedList = new LinkedList<>();
        linkedList.add(6);
        linkedList.add(4);
        linkedList.add(9);
        linkedList.add(8);
        linkedList.add(778);
        linkedList.add(5);
        linkedList.add(9);
        linkedList.add(778);
        linkedList.add(90);
        linkedList.add(13);
        linkedList.removeAt(3);
        linkedList.removeAll(9);
        linkedList.removeAll(778);
        linkedList.remove(13);
        for (int i = 0; i < linkedList.size(); i++) {
            System.out.println(linkedList.get(i));
        }

    }
}

