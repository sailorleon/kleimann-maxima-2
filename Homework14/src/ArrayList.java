package Homework14;


public class ArrayList<T> {

    private final static int DEFAULT_SIZE = 10;

    private T[] elements;

    private int size;


    public ArrayList() {
        // при создании списка я создал внутри массив на 10 элементов
        this.elements = (T[]) new Object[DEFAULT_SIZE];
    }

    private ArrayList(T[] elements) {
        this.elements = elements;
    }


    /**
     * Добавление элемента в конец списка
     *
     * @param element добавляемый элемент
     */
    public void add(T element) {
        // если у меня переполнен массив
        ensureSize();

        this.elements[size++] = element;
    }

    private void ensureSize() {
        if (isFullArray()) {
            resize();
        }
    }

    private void resize() {
        // создаем новый массив, который в полтора раза больше, чем предыдущий
        T[] newArray = (T[]) new Object[this.elements.length + elements.length / 2];
        // копируем элементы из старого массива в новый поэлементно
        for (int i = 0; i < size; i++) {
            newArray[i] = this.elements[i];
        }
        // заменяем ссылку на старый массив ссылкой на новый массив
        // старый массив будет удален java-машиной
        this.elements = newArray;
    }

    private boolean isFullArray() {
        return size == elements.length;
    }


    public static <T> ArrayList<T> empty() {
        return new ArrayList<T>(null);
    }


    public T get(int index) {
        if (isCorrectIndex(index)) {
            return (T) elements[index];
        } else
            //return (T) ArrayList.empty();
            throw new NullPointerException("Такого индекса нет в списке");
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    /**
     * Удаляет все элементы из списка
     */
    public void clear() {
        size = 0;
    }

    /**
     * Возвращает количество элементов списка
     *
     * @return размер списка
     */
    public int size() {
        return size;
    }

    /**
     * Удаляет элемент в заданном индексе, смещая элементы, которые идут после удаляемого на одну позицию влево
     * <p>
     * 0 -> [14] 1-> [71] 2-> [82] 3-> [25], size = 4
     * <p>
     * removeAt(1)
     * <p>
     * <p>
     * [14] [82] [25] | [25], size = 3
     *
     * @param index индекс удаляемого элемента
     */
    public void removeAt(int index) {
        T[] newArr = (T[]) new Object[size - 1];
        for (int i = 0, j = 0; i < size; i++) {
            if (i != index) {
                newArr[j] = elements[i];
                j++;
            }
        }
        elements = newArr;
        size = elements.length;
    }

    // public void removeAtTwo(int index) {
    //     for (int i = index + 1; i < size; i++) {
    //         elements[i - 1] = elements[i];
    //     }
    //     size--;
    //     elements[size] = 0;
    // }

    /**
     * Удаляет первое вхождение элемента в список
     * <p>
     * 34, 56, 78, 56, 92, 11
     * <p>
     * remove(56)
     * <p>
     * 34, 78, 56, 92, 11
     *
     * @param element
     */
    public void remove(T element) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(element)) {
                removeAt(i);
                break;
            }
        }
    }

    /**
     * Удаляет последнее вхождение элемента в список
     * <p>
     * 34, 56, 78, 56, 92, 11
     * <p>
     * removeLast(56)
     * <p>
     * 34, 56, 78, 92, 11
     *
     * @param element
     */
    public void removeLast(T element) {
        for (int i = size - 1; i >= 0; i--) {
            if (elements[i].equals(element)) {
                removeAt(i);
                break;
            }
        }
    }

    public void removeAll(T element) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(element)) {
                removeAt(i);
            }

        }
    }

    /**
     * Вставляет элемент в заданный индекс (проверяет условие, index < size)
     * Элемент, который стоял под индексом index сдвигается вправо (как и все остальные элементы)
     * 34, 56, 78, 56, 92, 11
     * <p>
     * add(2, 100)
     * <p>
     * 34, 56, 100, 78, 56, 92, 11
     *
     * @param index   куда вставляем элемент
     * @param element элемент, который будем вставлять
     */
    public void add(int index, T element) {
        if (isFullArray()) {
            resize();
        }

        for (int i = elements.length - 1; i >= index; i--) {
            elements[i] = elements[i - 1];
        }
        elements[index] = element;
        size++;
    }
}

