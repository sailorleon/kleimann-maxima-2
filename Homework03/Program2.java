import java.util.Scanner;

class Program2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);	
		
		int number = 0; // буферная переменная
    	int mincount = 0; // счетчик локальных минимумов
    	int inptNumber = 0; // переменная цикла

    	int left = 0;
    	int center = 0;
    	int right = 0;

		while (inptNumber != -1) {  // цикл чтения чисел с консоли, пока не введена -1

			number = scanner.nextInt();	//берем число с консоли
			left = number;
				System.out.println("1-е число: " + left);
			number = scanner.nextInt();	//берем следующее число с консоли
			center = number; 
				System.out.println("2-е число: " + center);
			number = scanner.nextInt();	//берем следующее число с консоли
			right = number; 
				System.out.println("3-е число: " + right);

			//проверка на локальный минимум из 3-х введенных чисел
			if ((left > center) & (center < right)) {
				mincount = mincount + 1; // Считаем количество локальных минимумов
				}

				number = scanner.nextInt();	//берем следующее число с консоли
				inptNumber = number; //меняем переменную цикла

			if (inptNumber != -1) {
				// смещаемся вправо по потоку вводимых чисел
				left = center;
				center = right;
				number = scanner.nextInt();	//берем следующее число с консоли, пока не введем -1
				right = number; 
				} else 
				System.out.println("Количество локальных минимумов: " + mincount);
		}
	}
}


// Program2 - необходимо найти количество локальных минимумов
// Локальный минимум, если выполняется условие - ai-1 > ai < ai+1
// 34
// 20
// 11 -> локальный минимум, потому что 20 > 11 < 15
// 15
// 17
// 8 -> локальный минимум, потому что 17 > 8 < 21
// 21
