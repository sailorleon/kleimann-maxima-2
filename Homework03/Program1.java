import java.util.Scanner;

class Program1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);	
		// int number = scanner.nextInt();
		int number = 0;
		int nNumber = 0;
		int cNumber = 0;
		int min = 1000000; // начальное значение миимальной суммы цифр

		while (number != -1) {
			
			number = scanner.nextInt();	//вводим число с консоли
			
			cNumber = number; // Взяли текущее число для расчета его суммы цифр

			// Вычисление суммы цифр числа 

			int digitsSum = 0; // Задали начальное значение суммы цифр числа

			// Находим сумму цифр числа

			while (cNumber != 0) {
				// begin of iteration
				int lastDigit = cNumber % 10;
				cNumber = cNumber / 10;
				digitsSum = digitsSum + lastDigit;
				// end of iteration
				}

			// System.out.println("Сумма цифр числа: " + digitsSum); // Вывели сумму цифр числа
			
			// Находим число с минимальной суммой цифр, исключая введенную в конце списка -1

			if (digitsSum < min & number != -1) {
				min = digitsSum; // Запомнили минимальную сумму цифр
				nNumber = number; // Запомнили введенное число
			}
		}
		System.out.println("Минимальная сумма цифр = " + min + " у числа " + nNumber);// вывод числа с минимальной суммой цифр
	}
}

// СДЛАНО

//На вход подается последовательность чисел:

//a0, a1, a2, a3, ... aN
//При этом N -> infinity, aN = -1

// Program1 - необходимо найти число, у которого сумма цифр минимальная среди всех остальных.

// 34 - 3 + 4 = 7
// 1112 - 1 + 1 + 1 + 2 = 5
// 2321 - 2 + 3 + 2 + 1 = 8
// 12321 - 1 + 2 + 3 + 2 + 1 = 9
// ОТВЕТ: 1112