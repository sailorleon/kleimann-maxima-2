import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


public class FileInput {
    public static void main(String[] args) {
        try {


            Path p = Paths.get("C:\\Users\\Maria\\IdeaProjects\\MaximaTestHomework\\Song");
            List<String> read = Files.readAllLines(p);

            List<String> words = new ArrayList<>();
            for (String oneRow : read) {
                String[] mass = oneRow.split("(, )|(,)|( )|(!)");
                for (String oneWord : mass) {
                    words.add(oneWord.toLowerCase());
                }
            }


            int max = 0;
            String maxElem = "";

            HashMap<String, Integer> map = new HashMap<String, Integer>();

            for (String currentWord : words) {
                if (map.containsKey(currentWord)) {
                    map.put(currentWord, map.get(currentWord) + 1);
                } else {
                    map.put(currentWord, 1);
                }

                if (map.get(currentWord) > max) {
                    max = map.get(currentWord);
                    maxElem = currentWord;
                }
            }

            System.out.println("The word that has been encountered the most number of times is - '" + maxElem + "' -  it has been encountered - " + max + " times");
            System.out.println("");


            for (Map.Entry entry : map.entrySet()) {
                System.out.println(entry.getKey() + " - " + entry.getValue());
            }


        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
