import java.util.Scanner;

public class Program8 {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите целое число: ");
            System.out.println("Для выхода из программы введите -1. ");
            int number = scanner.nextInt();

            while (number != -1) {
                // вызов функции, передача аргумента number в формальный параметр x, получение значения
                // и присваивание этого значения переменной digitsSum
                int digitsSum = sumOfDigits(number);
                System.out.println("Сумма цифр числа: " + digitsSum);
                number = scanner.nextInt();
            }
        }
        // sumOfDigits - название функции
        // int x - формальный параметр
        public static int sumOfDigits(int x) {
            // тело функции
            if (x < 10) return x;
                else {
                    int sum = x%10 + sumOfDigits(x/10);
                    return sum;
            }
        }
}