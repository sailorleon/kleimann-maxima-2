import java.util.Scanner;

class Program1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int number = 0;
		int age = 0;
		int count = 0;

		// считали с консоли размер массива
		System.out.println("Введите размер массива: ");
		int size = scanner.nextInt();
		// размер массива определяется в момент выполнения программы
		int[] array = new int[size]; // определение длины массива
		int[] agesCount = new int[size]; // еще один массив для подсчета людей с одинаковым возрастом

		//Вводим значения массива с консоли
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
		// Выводим значения массива array
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		// для каждого элемента массива array считаем сколько раз он встречается и заполняем массив agesCount
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < agesCount.length; j++){
				if (array[j] == array[i]) {
					agesCount[i] = agesCount[i] + 1;
				}
			}
		}
		// Выводим значения массива agesCount
		for (int i = 0; i < agesCount.length; i++) {
			System.out.println(agesCount[i]);
		}
		// Ишем в массиве agesCount индекс элемента с максимальным значением
		for (int i = 0; i < agesCount.length; i++) { // пробегаем по элементам массива
			for (int j = i; j < agesCount.length; j++) { // срвниваем элемент массива с другими элементами
				if (agesCount[j] > agesCount[i]) { // если найден больший элемент, чем текущий элемент массива
					count = agesCount[j]; // сколько раз встретился такой возраст
					age = array[j]; // значение возраста
				}
			}
		}
		System.out.println("Возраст " + age + " встретился чаще всего.");
		System.out.println("Количество людей с возрастом " + age + " составляет: " + count);
	}
}


// На вход подается список возрастов людей (мин 0 лет, макс 120 лет)
// Необходимо вывести информацию о том, какой возраст чаще всего встречается.

// 45
// 30
// 30
// 45
// 45
// 30
// 30
// 12
// -1

// Вывод - 30 лет
