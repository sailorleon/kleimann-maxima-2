package Homework25;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Main {

    private static final String SQL_SELECT_FROM_FOOD_PRODUCTS = "select *" +
            " from food_products order by id";

    public static void main(String[] args) {
        Properties properties;

        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        try (Connection connection = DriverManager.getConnection(
                properties.getProperty("db.url"),
                properties.getProperty("db.user"),
                properties.getProperty("db.password"));
             Statement statement = connection.createStatement()) {

            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_FROM_FOOD_PRODUCTS)) {
                while (resultSet.next()) {
                    String name = resultSet.getString("name");
                    Integer price = resultSet.getInt("price");
                    String vendor = resultSet.getString("vendor");



                    System.out.println(name + " " + price + " " + vendor);
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}


