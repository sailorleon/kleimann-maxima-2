package Homework25;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

/**
 * 16.02.2022
 * 28. JDBC
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class MainGood {

    //language=SQL
    private static final String SQL_SELECT_FROM_PEOPLE_DATA = "select *" +
            " from people_data order by id";

    public static void main(String[] args) {
        Properties properties;
        //открывает саму базу на подключение
        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        //создали объект для подключения к базе
        try (Connection connection = DriverManager.getConnection(
                properties.getProperty("db.url"),
                properties.getProperty("db.user"),
                properties.getProperty("db.password"));
             Statement statement = connection.createStatement()) { //  создали объект для отправки запроса в базу

            // cчитали из базы данные
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_FROM_PEOPLE_DATA)) {
                while (resultSet.next()) {
                    String firstName = resultSet.getString("first_name");
                    String lastName = resultSet.getString("last_name");
                    String country = resultSet.getString("country");
                    String gender = resultSet.getString("gender");
                    String profession = resultSet.getString("profession");
                    String email = resultSet.getString("email");


                    System.out.println(firstName + " " + lastName + " " + country + " " + gender + " " + profession + " " + email);
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

