import java.util.Scanner;

public class Program {
    // процедура сортировки массива
    public static void selectionSort(int[] array){
        int min, indexOfMin;
        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexOfMin = i;
            // пробегаем все элементы массива
            for (int j = i; j < array.length; j++) {
                // как только нашли новый минимальный
                if (array[j] < min) {
                    // то запоминаем его значение и индекс
                    min = array[j];
                    indexOfMin = j;
                }
            }
            // как только нашли минимальный элемент, меняем его местами с самым первым
            int temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;
        }
    }
    // функция boolean search(int[] array)
    static boolean search(int[] array){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число: ");
        int element = scanner.nextInt(); // считали искомый элемент
        boolean hasElement = false;
        int left = 0;
        int right = array.length -1;
        int middle = left + (right - left) / 2;
        while (left <= right) {
            if (element < array[middle]) {
                right = middle - 1;
            } else if (element > array[middle]) {
                left = middle + 1;
            } else {
                hasElement = true;
                break;
            }
            middle = left + (right - left) / 2;
        }
        System.out.println(hasElement);
        return hasElement;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // запрос размера массива
        System.out.println("Введите размер массива: ");
        int size = scanner.nextInt();
        int[] array = new int[size]; // объявили массив
        for (int i = 0; i < array.length; i++) {  // Вводим значения массива с консоли
        array[i] = scanner.nextInt();
        }
        for (int i = 0; i < array.length; i++) {  // Выводим значения массива array
            System.out.print(array[i] + " ");
        }
        // Сортировка массива, использовать процедуру void selectionSort(int[] array)
        // вызов процедуры сортировки
        selectionSort(array);
        System.out.println();
        for (int i = 0; i < array.length; i++) {  // Выводим отсортированный массив array
            System.out.print(array[i] + " ");
        }
        System.out.println();
        // Бинарный поиск числа среди элеметов массива, использовать функцию boolean search(int[] array)
        // вызов функции
        search(array);
    }
}
