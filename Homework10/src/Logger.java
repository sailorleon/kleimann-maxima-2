package Logger;

public class Logger {
    private final static Logger instance;

    private Logger() {
    }

    static {
        instance = new Logger();
    }

    public static Logger getInstance() {
        return instance;
    }

    public void log(String message) {
        System.out.println("The log message: " + message);
    }

}
