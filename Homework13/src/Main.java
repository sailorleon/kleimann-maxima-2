package example5;


/**
 * 20.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Box box = new Box(5, 5, 5, 10, 20, 30);
        Sphere sphere = new Sphere(0, 0, 0, 10);
        Point point = new Point(1, 1, 1);

        Shape[] shapes = {box, sphere};
        SpaceObject[] spaceObjects = {box, sphere, point};

        for (int i = 0; i < shapes.length; i++) {
            shapes[i].scale(0.5);
        }

        for (int i = 0; i < spaceObjects.length; i++) {
            spaceObjects[i].move(1, 1, 1);
        }

        System.out.println(box.getLength() + " " + box.getHeight() + " " + box.getWidth());
    }
}
