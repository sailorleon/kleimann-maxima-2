public class Driver {

    public String getName() {
        return name;
    }

    private String name;
    private int experience;
    private Bus bus;

    public Driver(String name, int experience) {
        this.name = name;
        if (experience > 0) {
            this.experience = experience;
        } else {
            System.out.println("ОПЫТА у " + name + " НЕТ!");
        }
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public void drive()  {
        bus.setDriving(true);
        bus.countPassengers();
        System.out.println("Автобус поехал");
    }

    public void stopsTheBus() {
        bus.setDriving(false);
        System.out.println("Водитель остановил автобус");
    }
}

