public class Main {
    public static void main(String[] args) throws InterruptedException {
        Driver driver1 = new Driver("Семен", 10); // Водитель Семен со стажем 10
        Driver driver2 = new Driver("Влад", 2); // Водитель Влад со стажем 2
        Driver driver3 = new Driver("Антон", 68); // Водитель Антон со стажем 68
        // Это экземпляры класса Driver

        Bus bus1 = new Bus(99, "Нефаз", 6); // Это создали экземпляр класса Bus с номером 99 модель Нефаз на 6 мест

        // Создаем Пассажиров с их именами
        Passenger passenger1 = new Passenger("Марсель");
        Passenger passenger2 = new Passenger("Виктор");
        Passenger passenger3 = new Passenger("Айрат");
        Passenger passenger4 = new Passenger("Сергей");
        Passenger passenger5 = new Passenger("Кирилл");
        Passenger passenger6 = new Passenger("Иван");
        Passenger passenger7 = new Passenger("Женя");

        passenger1.goToBus(bus1); // 1-й пассажир сел в Автобус
        passenger2.goToBus(bus1); //
        passenger3.goToBus(bus1); //
        passenger4.goToBus(bus1); //
        passenger5.goToBus(bus1); //
        passenger6.goToBus(bus1); // 6-й пассажир сел в Автобус

        bus1.setDriver(driver1); // Назанчили и посадили в Автобус Водителя

        System.out.println(driver1.getName() + " проводит перекличку"); // Проверка Имен и количества Пассажиров

        driver1.drive(); // Автобус поехал

        passenger1.leaveBus(); // 1-й Пассажир вышел из Автобуса

        passenger7.goToBus(bus1); // 7-й Пассажир сел в Автобус

        bus1.setDriver(driver2); // Назначен другой Водитель

        driver1.stopsTheBus(); // Автобус остановился

        bus1.setDriver(driver3); // Назначен другой Водитель

        System.out.println("Автобус поехал");

        driver3.stopsTheBus();

        passenger4.leaveBus();

        passenger4.goToBus(bus1);

        passenger6.leaveBus();
    }
}

