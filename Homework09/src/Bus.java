
// ЭТО автобус, у которого есть Номер int number и Модель String model
public class Bus {
    private int number;
    private String model;

    // это геттер, возвращает driver
    public Driver getDriver() {
        return driver;
    }

    private Driver driver;
    private boolean isMoving;
    // это логическая переменная, которая показывает состояние Автобуса - движется или стоит
    public boolean isMoving() {
        return isMoving;
    }

    // массив пассажиров
    private Passenger[] passengers;
    // фактическое количество пассажиров на данный момент
    private int count;

    // Это метод, который сажает Пассажира на его место в Автобусе
    public Bus(int number, String model, int placesCount) {
        if (number > 0) {
            this.number = number;
        } else {
            this.number = 1;
        }
        this.model = model;
        // создали placesCount объектных переменных в которые можно положить пассажира
        this.passengers = new Passenger[placesCount];
    }

    // Это метод попадания Пассажира в Автобус
    public void incomePassenger(Passenger passenger) {
        // проверяем, не превысили ли мы количество мест?
        if (!this.isFull()) {
            for (int i = 0; i < passengers.length; i++) {
                if (passengers[i] == null) {
                    passengers[i] = passenger;
                    System.out.println("Я, " + passenger.getName() + ", зашел в автобус");
                    break;
                }
            }
        } else {
            System.out.println("Автобус переполнен!");
        }
    }

    // Этот метод вкл или выкл двидение Автобуса
    public void setDriving(boolean driving) {
        if (driving) isMoving = true;
        else isMoving = false;

    }
    // Этот метод связывет Водителя и Автобус, то есть этот Водитель управляет этим Автобусом
    public void setDriver(Driver driver) {
        if (!isMoving) {
            this.driver = driver;
            driver.setBus(this);
            System.out.println("Новый водитель назначен - " + this.driver.getName());
        } else
            System.out.println("Нельзя менять водителя, пока автобус в движении");
    }

    // Это перекличка Пассажиров в Автобусе
    public void countPassengers() {
        for (int i = 0; i < passengers.length; i++) {
            System.out.println("Я, " + passengers[i].getName() + " здесь!");
        }
    }

    // Это метод "высадки" пассажира из автобуса
    public void discardPassenger(Passenger passenger) {
        for (int i = 0; i < passengers.length; i++) {
            if (passengers[i] != null && passengers[i].equals(passenger)) {
                passengers[i] = null;
            }
        }
    }

    // Это метод проверки, есть ли места в Автобусе перед посадкой Пассажира
    public boolean isFull() {
        int count = 0;
        for (Passenger passenger : passengers) {
            if (passenger != null) {
                count++;
            }
        }
        return count == passengers.length;
    }

}
