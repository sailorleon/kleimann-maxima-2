public class Passenger {
    private String name;

    // объектная переменная, поле, которое ссылается на какой-то автобус
    private Bus bus;
    private Passenger[] passengers;

    // перегруженные конструкторы
    public Passenger() {
        this.name = "Без имени";
    }

    public Passenger(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    // Этот метод помещает Пасажира в Автобус
    public void goToBus(Bus bus) {
        // проверяем, а не в автобусе ли мы уже?
        if (this.bus != null) {
            System.out.println("Я, " + name + ", уже в автобусе!");
        } else {
            // Если Автобус движется
            if (!bus.isMoving()) {
                // Если Автобус уже полный
                if (!bus.isFull()) {
                    // если автобуса еще не было
                    this.bus = bus;
                    // передаем в автобус себя
                    this.bus.incomePassenger(this);
                } else { //Автобус полон
                    System.out.println("Я, " + name + ", не попал в автобус(");
                }
            } else { // Автобус движется
                System.out.println("Нельзя войти в автобус во время его движения");
            }
        }
    }

    // Этот метод высаживает пассажира из Автобуса
    public void leaveBus () {
        if (!bus.isMoving()) {
            bus.discardPassenger(this);
            bus = null;
            System.out.println("Пассажир " + this.getName() + " покинул автобус");
        } else
            System.out.println("Пассажир захотел покинуть автобус. Нельзя покидать автобус во время его движения");


    }
}

